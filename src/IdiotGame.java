import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;



public class IdiotGame extends Application {

    private Pane pane = new Pane();
    private Button idiotBt;
    private Text title;
    private Rectangle box;
    private int count = 0;
    private boolean change = false;
    private Text tryAgain;





    public void start(Stage primaryStage) {

        title = new Text("How to entertain an Idiot");
        title.setY(30);
        title.setX(120);
        title.setFont(Font.font(30));
        pane.getChildren().add(title);

        box = new Rectangle(185, 180, 105, 65);
        box.setFill(Color.TRANSPARENT);
        pane.getChildren().add(box);

        idiotBt = new Button("Click Here!");
        idiotBt.setLayoutX(200);
        idiotBt.setLayoutY(200);
        pane.getChildren().add(idiotBt);


        box.setOnMouseMoved(e -> {

            double mouseX = e.getX();
            double mouseY = e.getY();
        if (!change) {
            if (mouseY > box.getY() + 5 && mouseY < box.getY() + box.getHeight() - 5) {

                if (pane.getWidth() > box.getX() + box.getWidth()) {
                    if (idiotBt.getLayoutX() > mouseX) {
                        box.setX(mouseX + 5);
                        idiotBt.setLayoutX(mouseX + 20);
                    }
                }

                if (pane.getLayoutX() < box.getX()) {
                    if (idiotBt.getLayoutX() < mouseX) {
                        box.setX(mouseX - 115);
                        idiotBt.setLayoutX(mouseX - 100);
                    }
                }
            }

            if (mouseX > box.getX() && mouseX < box.getX() + box.getWidth()) {

                if (pane.getHeight() > box.getY() + box.getHeight()) {
                    if (idiotBt.getLayoutY() > mouseY) {
                        box.setY(mouseY + 10);
                        idiotBt.setLayoutY(mouseY + 26);
                    }
                }

                if (pane.getLayoutY() < box.getY()) {
                    if (idiotBt.getLayoutY() < mouseY) {
                        box.setY(mouseY - 70);
                        idiotBt.setLayoutY(mouseY - 54);
                    }
                }
            }
        }
        if(change){
            pane.getChildren().remove(tryAgain);
            int moveX = (int) (Math.random() * 500);
            int moveY = (int) (Math.random() * 500);
            idiotBt.setLayoutX(moveX);
            idiotBt.setLayoutY(moveY);
            box.setX(moveX - 15);
            box.setY(moveY - 20);

        }
        });

        // Expert hard MODE!
//        idiotBt.setOnMouseEntered(e -> {
//            int moveX = (int) (Math.random() * 500);
//            int moveY = (int) (Math.random() * 500);
//            idiotBt.setLayoutX(moveX);
//            idiotBt.setLayoutY(moveY);
//            box.setX(moveX - 15);
//            box.setY(moveY - 20);
//        });


        idiotBt.setOnAction(event -> {
            count++;

            if( count == 1 ){
                change = true;
                tryAgain = new Text("Try Again");
                tryAgain.setFont(Font.font(60));
                tryAgain.setX(115);
                tryAgain.setY(100);
                tryAgain.toBack();
                pane.getChildren().add(tryAgain);
                reset();
            }
            if(count > 1) {
                change = false;
                pane.getChildren().remove(tryAgain);
                Text idiot = new Text("YOUR AN IDIOT!");
                idiot.setFont(Font.font(50));
                idiot.setStroke(Color.RED);
                idiot.setX(115);
                idiot.setY(100);
                idiot.toBack();
                pane.getChildren().add(idiot);
                pane.getChildren().remove(idiotBt);
                pane.getChildren().remove(box);
                reset();
            }

        });


        Scene scene = new Scene(pane, 550, 550);
        primaryStage.setTitle("Idiot Game");
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    public void reset(){
        box.setX(185);
        box.setY(180);
        idiotBt.setLayoutX(200);
        idiotBt.setLayoutY(200);
        pane.getChildren().remove(idiotBt);
        pane.getChildren().remove(box);
        pane.getChildren().add(box);
        pane.getChildren().add(idiotBt);

    }





}
